package main
import "fmt"

func bin_search(key int, nums []int, n int) int{
    var low = 0
    var mid = 0
    var high = n 
    
    for low <= high{
        mid = low + (high - low) / 2;
        
        if(nums[mid] > key){
            high = mid - 1
        }else{
            low = mid + 1
        }
    }
    return mid
}
func main() {
  nums := []int{1,2,3,4,5,6,7,8,9}
  fmt.Println(bin_search(7, nums, 9))
}